# Template Notes

- https://github.com/vitejs/vite/tree/create-vite%405.3.0/packages/create-vite/template-react-ts
- https://github.com/vitejs/vite/tree/create-vite%405.5.2/packages/create-vite/template-react-ts:
  - https://github.com/vitejs/vite/commit/6eab91e5011ec443179af5e58aa7c6123b2d445e
  - https://github.com/vitejs/vite/commit/efcd830e479092a0d3b95e0caf4a253d7835892c
  - https://github.com/vitejs/vite/pull/17774
  - https://devblogs.microsoft.com/typescript/announcing-typescript-5-6-beta/#tsbuildinfo-is-always-written
- https://gitlab.com/joaommpalmeiro/template-react-vike-static
- https://developer.mozilla.org/en-US/docs/Web/CSS/color-scheme: `color-scheme: light;`
- `tsc -b`: "Build a composite project in the working directory."
- https://mantine.dev/core/color-swatch/
- https://biomejs.dev/linter/rules/use-filenaming-convention/#options:
  - `"filenameCases": ["camelCase", "export"]`
  - "(...) or equal to the name of one export in the file."

## Commands

```bash
npm create vite@5.5.2 react-template -- --template react-ts
```

```bash
npx tsc --help
```

## Snippets

### Minimal `.npmrc`

```ini
# https://docs.npmjs.com/cli/v10/using-npm/config#config-settings
# https://docs.npmjs.com/cli/v10/using-npm/config#environment-variables

engine-strict=true
git-tag-version=false
package-lock=false
save-exact=true
```

### CSS reset

```css
:root {
  font-family: Inter, system-ui, Avenir, Helvetica, Arial, sans-serif;
  line-height: 1.5;
  font-weight: 400;

  color-scheme: light dark;
  color: rgba(255, 255, 255, 0.87);
  background-color: #242424;

  font-synthesis: none;
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
```

### `tsconfig.app.json` file

```json
{
  "extends": "create-vite-tsconfigs/react/tsconfig.app.json",
  "compilerOptions": {
    "tsBuildInfoFile": "./node_modules/.tmp/tsconfig.app.tsbuildinfo"
  },
  "include": ["src"]
}
```

### `tsconfig.node.json` file

```json
{
  "extends": "create-vite-tsconfigs/react/tsconfig.node.json",
  "compilerOptions": {
    "tsBuildInfoFile": "./node_modules/.tmp/tsconfig.node.tsbuildinfo"
  },
  "include": ["vite.config.ts"]
}
```
