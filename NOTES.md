# Notes

- https://gitlab.com/joaommpalmeiro/template-react-vite

## Commands

```bash
npm install \
react \
react-dom \
&& npm install -D \
@biomejs/biome \
@joaopalmeiro/biome-react-config \
@types/react \
@types/react-dom \
@vitejs/plugin-react \
create-vite-tsconfigs \
npm-run-all2 \
sort-package-json \
typescript \
vite
```

```bash
rm -rf node_modules/ && npm install
```

```bash
npm config ls -l
```

### Clean slate

```bash
rm -rf dist/ node_modules/
```
